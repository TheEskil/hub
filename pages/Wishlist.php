<div class="head-control">
 <a id="WishlistResetActivity" class="button positive"><span class="inner"><span class="label" nowrap="">Reset Activity</span></span></a>
 <a id="WishlistAddItem" class="button positive"><span class="inner"><span class="label" nowrap="">Add Wish</span></span></a>
 <a id="WishlistBookmarklet" href="javascript:(function(){var r=new XMLHttpRequest();r.open('GET','http://<?php echo $_SERVER['SERVER_ADDR']; ?>/api/wishlist/imdb/?token=<?php echo $_COOKIE['HubToken']; ?>&IMDb='+encodeURI(window.location.href),true);r.onreadystatechange=function(){if(r.readyState==4){if(r.status!=201){var errorObject=eval('('+r.responseText+')');alert(errorObject.error.message);return;}else{var successObject=eval('('+r.responseText+')');alert(successObject.error.message);}}return;};r.send();})();" class="button positive"><span class="inner"><span class="label" nowrap="">+ Wish</span></span></a>
 <a id="WishlistRefresh" class="button positive"><span class="inner"><span class="label" nowrap="">Refresh Wishlist</span></span></a>
</div>

<!--
javascript:(function() {
	var r = new XMLHttpRequest();
  	r.open('GET','http://<?php echo $_SERVER['SERVER_ADDR']; ?>/api/wishlist/imdb/?token=<?php echo $_COOKIE['HubToken']; ?>&IMDb=' + encodeURI(window.location.href), true);
  	r.onreadystatechange = function() {
  		if(r.readyState == 4) {
    		if(r.status != 201) {
      			var errorObject = eval('(' + r.responseText + ')');
      			alert(errorObject.error.message);
      			return;
    		}
    		else{
      			var successObject = eval('(' + r.responseText + ')');
      			alert(successObject.error.message);
    		}
    	}
    	
    	return;
  	};
  r.send();
})();
//-->
<?php 
$FulfilledWishes = json_decode($Hub->Request('wishlist/new/'));

if(is_array($FulfilledWishes)) {
	echo '
	<div class="head">Recently Fulfilled Wishes</div>
	
	<table id="tbl-wishlist">
	 <thead>
	 <tr>
	  <th style="width: 90px">Since</th>
	  <th>Title</th>
	  <th style="width: 30px">Year</th>
	  <th style="width: 375px">File</th>
	  <th style="width: 56px">&nbsp;</th>
	 </tr>
	 </thead>'."\n";
	 
	foreach($FulfilledWishes AS $Wish) {
		$ActionLink = '<img src="images/icons/search_dark.png" />';
		$File = 'Not Available';
		
		if($Wish->DownloadDate) {
			if($Wish->File) {
				$ActionLink = '<img src="images/icons/check.png" title="" />';
				$File = $Wish->File;
			}
			else {
				$File = 'File is missing or has been deleted';
				$ActionLink = '<a href="?Page=Search&Search='.urlencode($Wish->Title.' '.$Wish->Year).'"><img src="images/icons/search.png" /></a>';
			}
		}
		
		echo '
		<tr id="'.$Wish->ID.'">
		 <td>'.date('d.m.y H:i', $Wish->Date).'</td>
		 <td>'.$Wish->Title.'</td>
		 <td>'.$Wish->Year.'</td>
		 <td>'.ConcatFilePath($File).'</td>
		 <td style="text-align: center">
		  '.$ActionLink.'
		  <a href="http://www.youtube.com/results?search_query='.urlencode($Wish->Title.' '.$Wish->Year).'+trailer" target="_blank"><img src="images/icons/youtube.png" /></a>
		  <a id="WishlistDelete-'.$Wish->ID.'" rel="ajax"><img src="images/icons/delete.png" /></a>
		 </td>
		</tr>'."\n";
	}
	
	echo '</table><br />'."\n";
}
?>

<div class="head">Wishlist</div>

<table id="tbl-wishlist">
 <thead>
 <tr>
  <th style="width: 90px">Since</th>
  <th>Title</th>
  <th style="width: 30px">Year</th>
  <th style="width: 375px">File</th>
  <th style="width: 56px">&nbsp;</th>
 </tr>
 </thead>
 
<?php 
$Wishes = json_decode($Hub->Request('wishlist/'));

if(is_object($Wishes) && property_exists($Wishes, 'error')) {
	echo '
	<tr>
	 <td colspan="4">'.$Wishes->error->message.'</td>
	</tr>'."\n";
}
else if(is_array($Wishes)) {
	foreach($Wishes AS $Wish) {
		$ActionLink = '<img src="images/icons/search_dark.png" />';
		$File = 'Not Available';
		
		if($Wish->DownloadDate) {
			if($Wish->File) {
				$ActionLink = '<img src="images/icons/check.png" title="" />';
				$File = $Wish->File;
			}
			else {
				$File = 'File is missing or has been deleted';
				$ActionLink = '<a href="?Page=Search&Search='.urlencode($Wish->Title.' '.$Wish->Year).'"><img src="images/icons/search.png" /></a>';
			}
		}
		
		echo '
		<tr id="'.$Wish->ID.'">
		 <td>'.date('d.m.y H:i', $Wish->Date).'</td>
		 <td>'.$Wish->Title.'</td>
		 <td>'.$Wish->Year.'</td>
		 <td>'.ConcatFilePath($File).'</td>
		 <td style="text-align: center">
		  '.$ActionLink.'
		  <a href="http://www.youtube.com/results?search_query='.urlencode($Wish->Title.' '.$Wish->Year).'+trailer" target="_blank"><img src="images/icons/youtube.png" /></a>
		  <a id="WishlistDelete-'.$Wish->ID.'" rel="ajax"><img src="images/icons/delete.png" /></a>
		 </td>
		</tr>'."\n";
	}
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($Wishes);
}
?>
</table>