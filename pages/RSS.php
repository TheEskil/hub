<?php
$Movies = (filter_has_var(INPUT_GET, 'Movies')) ? TRUE : FALSE;
$RSSID = (filter_has_var(INPUT_GET, 'ID')) ? $_GET['ID'] : '';
$Category = (filter_has_var(INPUT_GET, 'Category')) ? '/'.urlencode($_GET['Category']) : '';

if($Movies) {
	$Entries = json_decode($Hub->Request('/rss/movies'));
	
	echo '
	<div class="head-control">
	 <a href="?Page=RSS&Movies" class="button neutral"><span class="inner"><span class="label" nowrap="">Show Movies</span></span></a>
	</div>
	
	<div class="head">RSS Movies</div>'."\n";
	
	if(is_object($Entries) && property_exists($Entries, 'error')) {
		echo '<div class="notification information">'.$Entries->error->message.'</div>'."\n";
	}
	else if(is_array($Entries)) {
		echo '
		<table width="100%">
		 <tr>'."\n";
		$ImageCounter = 1;
		foreach($Entries AS $Entry) {
			$MoviePoster = APP_PATH.'/images/posters/tmdb-'.$Entry->TheMovieDBID.'.jpeg';
			if(is_file($MoviePoster)) {
				$Parsed = ParseRelease($Entry->Title);
				
				echo '
				<td style="text-align: center">
				 <img src="../images/posters/tmdb-'.$Entry->TheMovieDBID.'.jpeg"><br />
				 <a href="http://www.themoviedb.org/movie/'.$Entry->TheMovieDBID.'" target="_blank">'.$Parsed['Title'].' ('.$Parsed['Year'].')</a><br />
				 <small>'.$Parsed['Quality'].'</small><br />
				 <a id="TorrentDownload-'.$Entry->ID.'" rel="ajax"><img src="images/icons/download.png" /></a>
				</td>'."\n";
				
				if($ImageCounter++ % 5 == 0) {
					echo '</tr><tr>';
				}
			}
		}
		
		echo '</table>';
	}
	else {
		echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
		
		echo '<br /><span style="font-weight: bold">Returned:</span>';
		var_dump($Entries);
	}
}
else {
	$Entries = json_decode($Hub->Request('/rss/'.$RSSID.$Category));
	
	echo '
	<div class="head-control">
	 <a href="?Page=RSS&Movies" class="button neutral"><span class="inner"><span class="label" nowrap="">Show Movies</span></span></a>
	</div>
	
	<div class="head">RSS '.ltrim($Category, '/').'</div>'."\n";
	
	if(is_object($Entries) && property_exists($Entries, 'error')) {
		echo '<div class="notification information">'.$Entries->error->message.'</div>'."\n";
	}
	else if(is_array($Entries)) {
		echo '
		<table width="100%">
		 <thead>
		 <tr>
		  <th style="width: 85px">Published</th>
		  <th>Title</th>
		  <th>Category</th>
		  <th style="width: 64px">&nbsp;</th>
		 </tr>
		 </thead>'."\n";
	
		foreach($Entries AS $Entry) {
			$Parsed = ParseRelease($Entry->Title);
			
			$TMDBIcon = $FavouriteIcon = '';
			switch($Parsed['Type']) {
				case 'TV':
					$FavouriteIcon = '<a href="?Page=Search&Search='.urlencode($Parsed['Title']).'"><img src="images/icons/heart_add.png" /></a>';
				break;
				
				case 'Movie':
					$TMDBIcon = '<a href="https://api.themoviedb.org/3/movie/'.$Entry->TheMovieDBID.'?api_key=cf62f88e0eab97fa4954199e89eb3b23" target="_blank"><img src="images/icons/information.png" /></a>';
				break;
			}
			
			echo '
			<tr>
			 <td>'.date('d.m.y H:i', $Entry->PubDate).'</td>
			 <td>'.$Entry->Title.'</td>
			 <td>
			  <a href="?Page=RSS&ID='.$_GET['ID'].'&Category='.urlencode($Entry->Category).'">'.$Entry->Category.'</a>
			 </td>
			 <td style="text-align: right">
			  '.$TMDBIcon.'
			  '.$FavouriteIcon.'
			  <a id="TorrentDownload-'.$Entry->ID.'" rel="ajax"><img src="images/icons/download.png" /></a>
			 </td>
			</tr>'."\n";
		}
		
		echo '</table>'."\n";
	}
	else {
		echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
		
		echo '<br /><span style="font-weight: bold">Returned:</span>';
		var_dump($Entries);
	}
}
?>