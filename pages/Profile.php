<div class="head">Profile Settings</div>

<table>
 <thead>
  <tr>
   <th colspan="2">E-mail</th>
  </tr>
 </thead>
 <tfoot>
  <tr>
   <td style="width: 200px">Your current e-mail address</td>
   <td><input type="text" /></td>
  </tr>
 </tfoot>
</table>

<br />

<table>
 <thead>
  <tr>
   <th colspan="2">Password</th>
  </tr>
 </thead>
 <tfoot>
  <tr>
   <td style="width: 200px">Your current password</td>
   <td><input type="password" /></td>
  </tr>
  <tr>
   <td>New password</td>
   <td><input type="password" /></td>
  </tr>
  <tr>
   <td>New password (repeat)</td>
   <td><input type="password" /></td>
  </tr>
 </tfoot>
</table>