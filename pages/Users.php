<div class="head">Users</div>

<?php
$Users = json_decode($Hub->Request('/users/'));

if(is_object($Users) && is_object($Users->error)) {
	echo '<div class="notification warning">'.$Users->error->message.'</div>'."\n";
}
else if(is_array($Users)) {
	echo '
	<table>
	 <thead>
	  <tr>
	   <th style="width: 130px">Name</th>
	   <th>E-mail</th>
	   <th style="width: 120px">Level</th>
	   <th style="width: 30px">Action</th>
	  </tr>
	 </thead>
	 <tfoot>'."\n";
	
	foreach($Users AS $User) {
		echo '
		<tr>
		 <td>'.$User->Name.'</td>
		 <td>'.$User->EMail.'</td>
		 <td>
		  <select>
		   <option>admin</option>
		   <option>user</option>
		   <option>guest</option>
		  </select>
		 </td>
		 <td>x</td>
		</tr>'."\n";
	}
	
	echo '
	 </tfoot>
	</table>'."\n";
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($Users);
}
?>

<br />

<div class="head">Tokens</div>

<?php
$Tokens = json_decode($Hub->Request('/users/tokens/'));

if(is_object($Tokens) && is_object($Tokens->error)) {
	echo '<div class="notification warning">'.$Tokens->error->message.'</div>'."\n";
}
else if(is_array($Tokens)) {
	echo '
	<table>
	 <thead>
	  <tr>
	   <th style="width: 115px">Last Activity</th>
	   <th style="width: 80px">User</th>
	   <th style="width: 70px">Address</th>
	   <th>Client</th>
	   <th style="width: 30px">Action</th>
	  </tr>
	 </thead>
	 <tfoot>'."\n";
	
	foreach($Tokens AS $Token) {
		echo '
		<tr>
		 <td>'.date('d.m.Y H:i:s', $Token->Active).'</td>
		 <td>'.$Token->Name.'</td>
		 <td>'.$Token->Address.'</td>
		 <td>'.$Token->Client.'</td>
		 <td>x</td>
		</tr>'."\n";
	}
	
	echo '
	 </tfoot>
	</table>'."\n";
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($Tokens);
}
?>