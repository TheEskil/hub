<?php
if(filter_has_var(INPUT_GET, 'ID')) {
	include_once APP_PATH.'/pages/SeriesDetailed.php';
}
else {
?>

<div class="head-control">
 <a id="SerieRefreshAll" class="button positive"><span class="inner"><span class="label" nowrap="">Refresh Series</span></span></a>
 <a id="EpisodesRebuild" class="button positive"><span class="inner"><span class="label" nowrap="">Rebuild Episodes</span></span></a>
 <a id="FoldersRebuild" class="button positive"><span class="inner"><span class="label" nowrap="">Rebuild Folders</span></span></a>
</div>

<div class="head">Series</div>
<?php
$Series = json_decode($Hub->Request('/series'));
 
if(is_object($Series) && is_object($Series->error)) {
	echo '<div class="notification information">'.$Series->error->message.'</div>'."\n";
}
else if(is_array($Series)) {
	echo '
	<table>
	 <thead>
	  <tr>
	   <th style="width: 50px">Since</th>
	   <th>Title</th>
	   <th style="width: 65px">First Aired</th>
	   <th style="width: 120px">Schedule</th>
	   <th style="width: 100px">Network</th>
	   <th style="width: 70px">Status</th>
	   <th style="width: 56px">&nbsp;</th>
	  </tr>
	 </thead>'."\n";
	 
	foreach($Series AS $Serie) {
		$TitleAlt = (!is_null($Serie->TitleAlt)) ? '/'.$Serie->TitleAlt : '';
			
		echo '
		<tr>
		 <td>'.date('d.m.y', $Serie->Date).'</td>
		 <td><a href="?Page=Series&ID='.$Serie->ID.'">'.$Serie->Title.$TitleAlt.'</a> ('.$Serie->EpisodeCount.' episodes)</td>
		 <td>'.date('d.m.y', $Serie->FirstAired).'</td>
		 <td>'.$Serie->AirDay.' '.$Serie->AirTime.'</td>
		 <td>'.$Serie->Network.'</td>
		 <td>'.$Serie->Status.'</td>
		 <td>
		  <a id="SerieRefresh-'.$Serie->ID.'" rel="ajax"><img src="images/icons/refresh.png" /></a>
		  <a onclick="javascript:$(\'tr[rel=Serie-'.$Serie->ID.']\').fadeToggle();"><img src="images/icons/spelling.png" /></a>
		  <a id="SerieDelete-'.$Serie->ID.'" rel="ajax"><img src="images/icons/delete.png" /></a>
		 </td>
		</tr>
		<tr rel="Serie-'.$Serie->ID.'" style="display:none;">
		 <td colspan="7">
		  <form id="SerieSpelling-'.$Serie->ID.'" action="/api/series/update/'.$Serie->ID.'?token='.$_COOKIE['HubToken'].'" method="post">
		   <strong>Alternative title for "'.$Serie->Title.'":</strong> <input name="TitleAlt" type="text" value="'.$Serie->TitleAlt.'" /> <input type="submit" value="Update" />
		  </form>
		 </td>
		</tr>'."\n";
	}
	
	echo '</table>'."\n";
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($Series);
}
}
?>