<div class="head">Schedule Log</div>

<?php
$Logs = json_decode($Hub->Request('log/schedule/'));

if(is_object($Logs) && is_object($Logs->error)) {
	echo '<div class="notification warning">'.$Logs->error->message.'</div>'."\n";
}
else if(is_array($Logs)) {
	echo '
	<table class="text-select">
	 <thead>
	 <tr>
	  <th width="70">Date</th>
	  <th width="100">Event</th>
	  <th>Text</th>
	 </tr>
	 </thead>'."\n";

	foreach($Logs AS $Log) {
		echo '
		<tr>
		 <td>
		  <span title="'.$Log->Address.'">'.date('d.m H:i', $Log->Date).'</span>
		 </td>
		 <td>'.$Log->Event.'</td>
		 <td>'."\n";
		
		$Text = json_decode($Log->Text);
		if(is_array($Text)) {
			echo '
			<table>
			 <thead>
			  <tr>
			   <th style="width: 100px">Time</th>
			   <th style="width: 200px">Title</th>
			   <th>Output</th>
			  </tr>
			 </thead>
			 <tfoot>'."\n";
			foreach($Text AS $Line) {
				if(is_object($Line->Output)) {
					$Output = $Line->Output->Code.' '.$Line->Output->Message;
				}
				else {
					$Output = $Line->Output;
				}
				
				echo '
				<tr>
				 <td>'.$Line->Time.'</td>
				 <td>'.$Line->Title.'</td>
				 <td>'.$Output.'</td>
				</tr>'."\n";
			}
			
			echo '
			 </tfoot>
			</table>'."\n";
		}
		else {
			echo $Log->Text;
		}
		
		echo '
		 </td>
		</tr>'."\n";
	}
	
	echo '</table>'."\n";
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($Logs);
}
?>