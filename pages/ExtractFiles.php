<script type="text/javascript">
$(document).ready(function() {
	$('a[id=ExtractOK]').click(function() {
		$(document).dequeue("ajaxRequests");
	});
});
</script>

<div class="head">Extract Files </div>

<?php
$CompletedFiles = json_decode($Hub->Request('/drives/files/completed'));

if(is_object($CompletedFiles) && property_exists($CompletedFiles, 'error')) {
	echo '<div class="notification warning">'.$CompletedFiles->error->message.'</div>'."\n";
}
else if(is_object($CompletedFiles)){
	echo 'Are you happy with these results? <a id="ExtractOK">Go ahead and execute them</a><br /><br />'."\n";
	
	echo '
	<table>
	 <thead>
	  <tr>
	   <th style="width: 30px; text-align: center;">Status</th>
	   <th style="width: 30px; text-align: center;">Action</th>
	   <th style="width: 48%">From</th>
	   <th style="width: 48%">To</th>
	  </tr>
	 </thead>
	 <tfoot>'."\n";
	 
	$FileNo = 0;
	if(is_object($CompletedFiles) && property_exists($CompletedFiles, 'Extract')) {
		foreach($CompletedFiles->Extract AS $File) {
			$FileNo++;
			
			$ExtractArr = array('File' => $File, 'debug' => TRUE);
			$ExtractFile = json_decode($Hub->Request('/drives/files/extract', 'POST', $ExtractArr));
			
			if(is_object($ExtractFile) && !property_exists($ExtractFile, 'error')) {
				echo '
				 <tr>
				  <td id="ExtractFile-'.$FileNo.'-Status" style="text-align: center">x</td>
				  <td style="text-align: center">Extract</td>
				  <td>'.$ExtractFile->From.'</td>
				  <td>'.$ExtractFile->To.'</td>
				 </tr>'."\n";
				
				echo '
				<script type="text/javascript">
				$(document).queue("ajaxRequests", function() {
					$.ajax({
						type: 	"post",
						url:    "/api/drives/files/extract?token='.$_COOKIE['HubToken'].'",
						data:   { File: "'.$File.'" },
						beforeSend: function() {
							$("#ExtractFile-'.$FileNo.'-Status").html("<img src=\"images/spinners/ajax-light.gif\" />");
						},
						success: function(data, textStatus, jqXHR) {
							$("#ExtractFile-'.$FileNo.'-Status").html("<img src=\"images/icons/check.png\" title=\"" + data.error.message + "\" />");
						
							$(document).dequeue("ajaxRequests");
						},
						error: function(jqXHR, textStatus, errorThrown) {
						    var responseObj = JSON.parse(jqXHR.responseText);
						    $("#ExtractFile-'.$FileNo.'-Status").html("<img src=\"images/icons/error.png\" title=\"" + responseObj.error.message + "\" />");
						}
					});
				});
				</script>'."\n";
			}
		}
	}
	
	if(is_object($CompletedFiles) && property_exists($CompletedFiles, 'Move')) {
		foreach($CompletedFiles->Move AS $File) {
			$FileNo++;
			
			$MoveArr = array('File' => $File, 'debug' => TRUE);
			$MoveFile = json_decode($Hub->Request('/drives/files/move', 'POST', $MoveArr));
			
			if(is_object($MoveFile) && !property_exists($MoveFile, 'error')) {
				echo '
				 <tr>
				  <td id="MoveFile-'.$FileNo.'-Status" style="text-align: center">x</td>
				  <td style="text-align: center">Move</td>
				  <td>'.$MoveFile->From.'</td>
				  <td>'.$MoveFile->To.'</td>
				 </tr>'."\n";
				
				echo '
				<script type="text/javascript">
				$(document).queue("ajaxRequests", function() {
					$.ajax({
						type: 	"post",
						url:    "/api/drives/files/move?token='.$_COOKIE['HubToken'].'",
						data:   { File: "'.$File.'" },
						beforeSend: function() {
							$("#MoveFile-'.$FileNo.'-Status").html("<img src=\"images/spinners/ajax-light.gif\" />");
						},
						success: function(data, textStatus, jqXHR) {
							$("#MoveFile-'.$FileNo.'-Status").html("<img src=\"images/icons/check.png\" title=\"" + data.error.message + "\" />");
						
							$(document).dequeue("ajaxRequests");
						},
						error: function(jqXHR, textStatus, errorThrown) {
						    var responseObj = JSON.parse(jqXHR.responseText);
						    $("#MoveFile-'.$FileNo.'-Status").html("<img src=\"images/icons/error.png\" title=\"" + responseObj.error.message + "\" />");
						}
					});
				});
				</script>'."\n";
			}
		}
	}
	
	echo '
	 </tfoot>
	</table>'."\n";
	
	echo '
	<script type="text/javascript">
	$(document).queue("ajaxRequests", function() {
		$.ajax({
			type: 	"get",
			url:    "/api/drives/clean",
			success: function(data, textStatus, jqXHR) {
				$(document).dequeue("ajaxRequests");
			}
		});
	});
	</script>'."\n";
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($CompletedFiles);
}
?>