<div class="head-control">
 <a id="LogScheduleToggle" class="button positive"><span class="inner"><span class="label" nowrap="">Toggle Schedule Logs</span></span></a>
</div>

<div class="head">Hub Log</div>

<?php
$Logs = json_decode($Hub->Request('log/'));

if(is_object($Logs) && is_object($Logs->error)) {
	echo '<div class="notification warning">'.$Logs->error->message.'</div>'."\n";
}
else if(is_array($Logs)) {
	echo '
	<table class="text-select">
	 <thead>
	 <tr>
	  <th width="70">Date</th>
	  <th width="100">Event</th>
	  <th>Text</th>
	 </tr>
	 </thead>'."\n";

	for($i = 0; $i < sizeof($Logs); $i++) {
		$TorrentsAdded = '';
		if($Logs[$i]->Event == 'RSS') {
			preg_match('/(Added) ([0-9]+) (torrents spread across) ([0-9]+) (RSS feeds)/', $Logs[$i]->Text, $LogPrevious);
			
			if(sizeof($LogPrevious)) {
				$TorrentsAdded = $LogPrevious[2];
				$RSSFeeds      = $LogPrevious[4];
				for($k = ($i + 1); $k < sizeof($Logs); $k++) {
					if($Logs[$k]->Event == 'RSS') {
						preg_match('/(Added) ([0-9]+) (torrents spread across) ([0-9]+) (RSS feeds)/', $Logs[$k]->Text, $LogNew);
						
						if(sizeof($LogNew)) {
							$ToDate         = $Logs[$i]->Date;
							$TorrentsAdded += $LogNew[2];
							$RSSFeeds       = ($LogNew[4] > $RSSFeeds) ? $LogNew[4] : $RSSFeeds;
						}
						continue;
					}
					else {
						$i = ($k - 1);
						break;
					}
				}
			}
		}
		else {
			$TorrentsAdded = 0;
			$ToDate = 0;
		}
		
		if($TorrentsAdded) {
			$Logs[$i]->Text = 'Added '.$TorrentsAdded.' torrents spread across '.$RSSFeeds.' RSS feeds';
		}
		
		$LogDateTo = !empty($ToDate) ? date('-d.m H:i', $ToDate) : '';
		
		if($Logs[$i]->Event == 'Schedule/Hub') {
			$Hide = ' id="Schedule-'.$i.'" style="display:none;"';
		}
		else {
			$Hide = ' id="Log-'.$i.'"';
		}
		
		echo '
		<tr'.$Hide.'>
		 <td>
		  <span title="'.$Logs[$i]->Address.'">'.date('d.m H:i', $Logs[$i]->Date).$LogDateTo.'</span>
		 </td>
		 <td>'.$Logs[$i]->Event.'</td>
		 <td>'.$Logs[$i]->Text.'</td>
		</tr>'."\n";
	}
	
	echo '</table>'."\n";
}
else {
	echo '<div class="notification failure">Failed to decipher data returned by API</div>'."\n";
	
	echo '<br /><span style="font-weight: bold">Returned:</span>';
	var_dump($Logs);
}
?>