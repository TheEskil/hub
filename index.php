<?php
ini_set('max_execution_time', (60 * 60 * 5));

require_once './resources/config.php';
require_once APP_PATH.'/api/resources/functions.php';
require_once APP_PATH.'/resources/api.hub.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html> 
<head>
 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
 <title>Hub</title>
 
<?php
if(!filter_has_var(INPUT_COOKIE, 'HubToken')) {
	echo '<link type="text/css" rel="stylesheet" href="css/login.css" />'."\n";
}
else {
	$Hub->SetToken($_COOKIE['HubToken']);
	$Token = json_decode($Hub->Request('/users/validate/'));
	
	if(is_object($Token) && is_object($Token->error)) {
		if($Token->error->code == 401) {
			if(filter_has_var(INPUT_COOKIE, 'HubToken')) {
				setcookie('HubToken', '', time() - 3600);
			}
			
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
	}
	
	echo '
	<link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
	<link type="text/css" rel="stylesheet" href="css/colorbox.css" />
	<link type="text/css" rel="stylesheet" href="css/fineuploader.css" />'."\n";
}
?>
 
 <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
 <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
 <script type="text/javascript" src="js/jquery.form.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/header.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/util.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/button.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/ajax.requester.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/deletefile.ajax.requester.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/handler.base.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/window.receive.message.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/handler.form.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/handler.xhr.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/uploader.basic.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/dnd.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/uploader.js"></script>
 <script type="text/javascript" src="js/valums-file-uploader/jquery-plugin.js"></script>
 
 <script type="text/javascript" src="js/noty/jquery.noty.js"></script>
 <script type="text/javascript" src="js/noty/layouts/topRight.js"></script>
 <script type="text/javascript" src="js/noty/layouts/center.js"></script>
 <script type="text/javascript" src="js/noty/themes/default.js"></script>
 <script type="text/javascript" src="js/noty/themes/hub.js"></script>
 
 <!--<link rel="shortcut icon" href="images/favicon.ico" />//-->
 <link rel="apple-touch-icon" href="images/logo-iphone.png" />
 <link rel="apple-touch-icon" sizes="72x72" href="images/logo-ipad.png" />
 <link rel="apple-touch-icon" sizes="114x114" href="images/logo-iphone4.png" />
 <link rel="apple-touch-icon" sizes="144x144" href="images/logo-ipad3.png" />
</head>

<body>
<div id="hidden"></div>

<?php
if(!filter_has_var(INPUT_COOKIE, 'HubToken')) {
?>
<script type="text/javascript">
$(document).ready(function() {
	function setcookie(name, value, days) {
	    if(days) {
	        var date = new Date();
	        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
	        var expires = '; expires=' + date.toGMTString();
	    }
	    else {
	    	var expires = '';
	    }
	    
	    document.cookie = name + '=' + value + expires + '; path=/;';
	}
	
	$('form[id="login"]').ajaxForm({
		error: function(responseText, statusText, xhr, $form) {
			console.log(responseText);
		},
		success: function(responseText, statusText, xhr, $form) {
			setcookie('HubToken', responseText, '365');
			
			document.location.reload(true);
		}
	});
});
</script>

<div id="login"> 
 <a href="http://hubapp.net/"><img src="images/login/logo.png" /></a> 

 <div class="box">
  <form id="login" method="post" action="/api/users/token">
   <h2>Login</h2>
   <p><label>Username:</label><input maxlength="64" class="text" type="text" name="Name" /></p> 
   <p><label>Password:</label><input maxlength="64" class="text" type="password" name="Password" /></p>
   <p>Remember me: <input type="checkbox" name="Persistent" /></p>
   <p><input class="submit" type="submit" name="submit" value="Sign in" /></p> 
   <div class="break"></div> 
  </form>
 </div> 
	
 <div id="footer">
  <a href="http://twitter.com/realhubapp/">Follow @realhubapp on Twitter</a>   
 </div> 

</div>
<?php
}
else {
?>
<script type="text/javascript">
$(document).ready(function() {
	UpdateBadge('Wishlist');
	UpdateBadge('UTorrent');
	UpdateBadge('RSS');
	
	$('tr[id|="multiple"]').click(function() {
		if($(this).find('input:checkbox').attr('checked') == 'checked') {
			$(this).css('font-weight', 'normal');
			$(this).find('input:checkbox').removeAttr('checked');
		}
		else if($(this).find('input:checkbox')[0]) {
			console.log($(this).find('input:checkbox')[0]);
			$(this).css('font-weight', 'bold');
			$(this).find('input:checkbox').attr('checked', 'checked');
	    }
	});
	
	$('a[id|="SelectAll"]').click(function() {
		Action   = $(this).attr('id').split('-');
		SecondID = Action[2];
		FirstID  = Action[1];
		
		if($(this).text() == 'Select All') {
			$(this).text('Deselect All');
			Action = 'select';
		}
		else {
			$(this).text('Select All');
			Action = 'deselect';
		}
		
		$('tr[id|="multiple-' + FirstID + '"]').each(function(index) {
			if($(this).find('input:checkbox')[0]) {
			if(Action == 'select') {
				$(this).css('font-weight', 'bold');
				$(this).find('input:checkbox').attr('checked', 'checked');
			}
			else {
				$(this).css('font-weight', 'normal');
				$(this).find('input:checkbox').removeAttr('checked');
			}
			}
		});
	});
	
	$('#jquery-wrapped-fine-uploader').fineUploader({
		debug: false,
		listElement: $('#hidden'),
		request: {
	    	endpoint: 'file-upload.php'
		},
		text: {
			uploadButton: 'Upload files',
			dragZone: 'Drop here',
		},
		failedUploadTextDisplay: {
			mode: 'custom',
			maxChars: 150,
		}
	}).on('complete', function(event, id, name, response) {
		if(response.error) {
			HubNotify('error', name + ' failed to upload. ' + response.error);
		}
		else {
			HubNotify('success', name + ' uploaded successfully');
		}
	});
	
	$('a[rel=trailer]').colorbox({iframe:true, width:600, height:500, current: 'Trailer {current} of {total}'});
	
	$('form[id|="SerieSpelling"]').ajaxForm({
		error: function(responseText, statusText, xhr, $form) {
			HubNotify('error', responseText.error.message);
		},
		success: function(responseText, statusText, xhr, $form) {
			HubNotify('success', responseText.error.message);
		}
	});
	
	$('#IconProfile').mouseover(function() {
		$(this).attr('src', 'images/icons/profile.png');
	}).mouseout(function() {
		$(this).attr('src', 'images/icons/profile_dark.png');
	});
	
	$('#IconSettings').mouseover(function() {
		$(this).attr('src', 'images/icons/settings.png');
	}).mouseout(function() {
		$(this).attr('src', 'images/icons/settings_dark.png');
	});
	
	$('#IconLogout').mouseover(function() {
		$(this).attr('src', 'images/icons/logout.png');
	}).mouseout(function() {
		$(this).attr('src', 'images/icons/logout_dark.png');
	});
	
	$('#IconUsers').mouseover(function() {
		$(this).attr('src', 'images/icons/users.png');
	}).mouseout(function() {
		$(this).attr('src', 'images/icons/users_dark.png');
	});
	
	$('div[id|="Cover"]').mouseover(function() {
		$('#CoverControl-' + $(this).attr('id').split('-')[1]).css('display', 'block');
	}).mouseout(function() {
		$('#CoverControl-' + $(this).attr('id').split('-')[1]).css('display', 'none');
	});
	
	$('a[rel="ajax"]').click(function() {
		Action   = $(this).attr('id').split('-');
		SecondID = Action[2];
		FirstID  = Action[1];
		Action   = Action[0];
		OriginalImg  = $(this).html();
		ImageObj = this;
		
		switch(Action) {
			case 'TorrentStart':
				AjaxImage('utorrent/start/' + FirstID, ImageObj, OriginalImg);
			break;
			
			case 'TorrentStop':
				AjaxImage('utorrent/stop/' + FirstID, ImageObj, OriginalImg);
			break;
			
			case 'TorrentPause':
				AjaxImage('utorrent/pause/' + FirstID, ImageObj, OriginalImg);
			break;
			
			case 'TorrentDelete':
				AjaxImage('utorrent/remove/' + FirstID, ImageObj, OriginalImg);
			break;
			
			case 'TorrentDeleteData':
				AjaxImage('utorrent/remove/data/' + FirstID, ImageObj, OriginalImg);
			break;
			
			case 'SerieRefresh':
				AjaxImage('series/refresh/' + FirstID, ImageObj, OriginalImg);
			break;
			
			case 'SerieDelete':
				noty({
					layout: 'topRight',
					text: 'Do you want to delete this serie?',
					buttons: [{
						addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
							AjaxImage('series/' + FirstID, ImageObj, OriginalImg, 'delete');
							$noty.close();
						}
					},
					{
						addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
							$noty.close();
						}
				    }]
				});
			break;
			
			case 'FileDelete':
				console.log(Action + ' ' + $(this).attr('id').replace(new RegExp(Action + '-', 'i'), ''));
			break;
			
			case 'WishlistDelete':
				noty({
					layout: 'topRight',
					text: 'Do you want to delete this wish?',
					buttons: [{
						addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
							AjaxImage('wishlist/' + FirstID, ImageObj, OriginalImg, 'delete');
							$noty.close();
						}
					},
					{
						addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
							$noty.close();
						}
				    }]
				});
			break;
			
			case 'DriveRemove':
				noty({
					layout: 'topRight',
					text: 'Do you want to remove this drive?',
					buttons: [{
						addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
							AjaxImage('drives/' + FirstID, ImageObj, OriginalImg, 'delete');
							$noty.close();
						}
					},
					{
						addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
							$noty.close();
						}
				    }]
				});
			break;
			
			case 'TorrentDownload':
				if(SecondID) {
					SecondID = '/' + SecondID;
				}
				else {
					SecondID = '';
				}
				
				AjaxImage('rss/download/' + FirstID + SecondID, ImageObj, OriginalImg);
			break;
			
			case 'FeedDelete':
				noty({
					layout: 'topRight',
					text: 'Do you want to delete this feed?',
					buttons: [{
						addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
							AjaxImage('rss/' + FirstID, ImageObj, OriginalImg, 'delete');
							$noty.close();
						}
					},
					{
						addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
							$noty.close();
						}
				    }]
				});
			break;
			
			case 'DriveActive':
				AjaxImage('drives/active/' + FirstID, ImageObj, OriginalImg);
			break;
			
			default:
				console.log(Action + ' ' + FirstID + ' default action');
		}
	});
	
	$('a[class*="button"]').click(function() {
		ID = $(this).attr('id');
		ButtonObj = this;
		ButtonVal = $(this).contents().find('.label').text();
		
		if($(this).hasClass('regular'))  ButtonClass = 'regular';
		if($(this).hasClass('positive')) ButtonClass = 'positive';
		if($(this).hasClass('negative')) ButtonClass = 'negative';
		if($(this).hasClass('blue'))     ButtonClass = 'blue';
		if($(this).hasClass('neutral'))  ButtonClass = 'neutral';
		if(!ButtonClass)                 ButtonClass = 'positive';
		
		if(ID) {
			Pattern = /[A-z]+\-[0-9]+/i;
			Result  = Pattern.test(ID);
			
			if(Result) {
				Action = ID.split('-');
				ID = Action[1];
				Action = Action[0];
				
				switch(Action) {
					case 'SerieAdd':
						AjaxButton('series/add/' + ID, ButtonObj, 'Adding ...', ButtonClass, ButtonVal);
					break;
					
					case 'SerieRefresh':
						AjaxButton('series/refresh/' + ID, ButtonObj, 'Refreshing ...', ButtonClass, ButtonVal);
					break;
					
					case 'SerieSpelling':
						console.log(Action);
					break;
					
					case 'SerieDelete':
						noty({
							layout: 'topRight',
							text: 'Do you want to delete this serie?',
							buttons: [{
								addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
									AjaxButton('series/' + ID, ButtonObj, 'Deleting ...', ButtonClass, ButtonVal, 'delete');
									$noty.close();
								}
							},
							{
								addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
									$noty.close();
								}
						    }]
						});
					break;
					
					case 'TorrentDownload':
						AjaxButton('rss/download/' + ID, ButtonObj, 'Downloading ...', ButtonClass, ButtonVal);
					break;
					
					default:
						console.log(Action + ' default action');
				}
			}
			else {
				switch(ID) {
					case 'WishlistBookmarklet':
						noty({
							layout: 'center',
							type: 'information',
							text: 'Drag this button to your bookmark bar to add wishlist items from IMDb pages automatically',
							timeout: 5000,
							theme: 'hub'
						});
					break;
					
					case 'SerieRefreshAll':
						AjaxButton('series/refresh/all', ButtonObj, 'Refreshing ...', ButtonClass, ButtonVal);
					break;
					
					case 'EpisodesRebuild':
						AjaxButton('series/rebuild/episodes', ButtonObj, 'Rebuilding ...', ButtonClass, ButtonVal);
					break;
					
					case 'FoldersRebuild':
						AjaxButton('series/rebuild/folders', ButtonObj, 'Rebuilding ...', ButtonClass, ButtonVal);
					break;
					
					case 'WishlistResetActivity':
						AjaxButton('wishlist/reset', ButtonObj, 'Resetting ...', ButtonClass, ButtonVal);
					break;
					
					case 'WishlistAddItem':
						WishlistID = randomString();
						
						$('#tbl-wishlist tbody tr:first').before(
						    '<tr id="' + WishlistID + '">' +
						     '<td>Now</td>' +
						     '<td><input name="Title" style="width:250px" type="text" /></td>' +
						     '<td><input name="Year" style="width:30px" type="text" value="<?php echo date("Y"); ?>"/></td>' +
						     '<td> </td>' +
						     '<td id="action-' + WishlistID + '" style="text-align:right">' +
						      '<a onclick="javascript:AjaxPost(\'WishlistAddItem\', \'' + WishlistID + '\');"><img src="images/icons/add.png" /></a>' +
						      '<a onclick="javascript:$(\'#' + WishlistID + '\').remove();"><img src="images/icons/delete.png" /></a>' +
						     '</td>' +
						    '</tr>');
					break;
					
					case 'WishlistRefresh':
						AjaxButton('wishlist/refresh', ButtonObj, 'Refreshing ...', ButtonClass, ButtonVal);
					break;
					
					case 'LogScheduleToggle':
						$('tr[id|="Schedule"]').toggle();
						$('tr[id|="Log"]').toggle();
					break;
					
					case 'DriveAdd':
						DriveID = randomString();
						
						$('#tbl-drives tbody tr:first').before(
						    '<tr id="' + DriveID + '">' +
						     '<td>Now</td>' +
						     '<td><input name="Share" style="width:250px" type="text" /></td>' +
						     '<td><input name="User" style="width:30px" type="text" /></td>' +
						     '<td><input name="Password" style="width:30px" type="text" /></td>' +
						     '<td><input name="Mount" style="width:30px" type="text" /></td>' +
						     '<td><input name="IsNetwork" type="hidden" value="0" />0</td>' +
						     '<td>0</td>' +
						     '<td>0</td>' +
						     '<td id="action-' + DriveID + '" style="text-align:right">' +
						      '<a onclick="javascript:AjaxPost(\'DriveAddItem\', \'' + DriveID + '\');"><img src="images/icons/add.png" /></a>' +
						      '<a onclick="javascript:$(\'#' + DriveID + '\').remove();"><img src="images/icons/delete.png" /></a>' +
						     '</td>' +
						    '</tr>');
					break;
					
					case 'TorrentStartAll':
						AjaxButton('utorrent/start/all', ButtonObj, 'Starting ...', ButtonClass, ButtonVal);
					break;
					
					case 'TorrentPauseAll':
						AjaxButton('utorrent/pause/all', ButtonObj, 'Pausing ...', ButtonClass, ButtonVal);
					break;
					
					case 'TorrentStopAll':
						AjaxButton('utorrent/stop/all', ButtonObj, 'Stopping ...', ButtonClass, ButtonVal);
					break;
					
					case 'TorrentRemoveFinished':
						AjaxButton('utorrent/remove/finished', ButtonObj, 'Removing ...', ButtonClass, ButtonVal);
					break;
					
					case 'TorrentRemoveAll':
						AjaxButton('utorrent/remove/data/all', ButtonObj, 'Removing ...', ButtonClass, ButtonVal);
					break;
					
					case 'RSSUpdate':
						AjaxButton('rss/refresh', ButtonObj, 'Updating ...', ButtonClass, ButtonVal);
					break;
					
					case 'RSSAddFeed':
						FeedID = randomString();
						
						$('#tbl-feeds tbody tr:first').before(
						    '<tr id="' + FeedID + '">' +
						     '<td>Now</td>' +
						     '<td><input name="Title" style="width:250px" type="text" /></td>' +
						     '<td><input name="Feed" style="width:300px" type="text" /></td>' +
						     '<td id="action-' + FeedID + '" style="text-align:right">' +
						      '<a onclick="javascript:AjaxPost(\'FeedAddItem\', \'' + FeedID + '\');"><img src="images/icons/add.png" /></a>' +
						      '<a onclick="javascript:$(\'#' + FeedID + '\').remove();"><img src="images/icons/delete.png" /></a>' +
						     '</td>' +
						    '</tr>');
					break;
					
					case 'DeleteSelectedEpisodes':
						$('#episodes').find('input:checkbox').each(function(index) {
							if($(this).attr('checked') == 'checked') {
								AjaxButton('series/episodes/' + $(this).attr('name'), ButtonObj, 'Deleting ...', ButtonClass, ButtonVal, 'DELETE');
							}
						});
					break;
					
					default:
						console.log(ID + ' default');
				}
			}
		}
	});
	
	$('#search').focus(function() {
		$('#search').animate({ width:'400px' }, { queue: false, duration: 200 });
		
		$('#search').blur(function() {
			if($(this).attr('value') == '') {
				$(this).attr('placeholder', 'Search ...');
				
				$(this).animate({ width:'100px' }, { queue: false, duration: 200 });
			}
		});
	});
	
	$('#search').keypress(function(event) {
		if(event.which == '13') {
			event.preventDefault();
			
			if($(this).attr('value') != 'Search ...' && $(this).attr('value') != '') {
				window.location = '?Page=Search&Search=' + escape($(this).attr('value'));
			}
	   	}
	});
});

function GetCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(';');

	for(i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0,ARRcookies[i].indexOf('='));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf('=') + 1);
		x = x.replace(/^\s+|\s+$/g, '');

		if(x == c_name) {
			return unescape(y);
		}
	}
}

function UpdateBadge(Badge) {
	switch(Badge) {
		case 'Wishlist':
			$.ajax({
				method: 'get',
				url:    'api/wishlist/badge?token=' + GetCookie('HubToken'),
				
				success: function(html) {
					if(html.Complete && html.Incomplete) {
						$('#WishlistBadge').html('<span class="badge dual rightbadge blue-badge">' + html.Incomplete + '</span><span class="badge dual leftbadge red-badge">' + html.Complete + '</span>');
					}
					else if(html.Incomplete) {
						$('#WishlistBadge').html('<span class="badge single blue-badge">' + html.Incomplete + '</span>');
					}
					else if(html.Complete) {
						$('#WishlistBadge').html('<span class="badge single red-badge">' + html.Complete + '</span>');
					}
				},
				error: function() {
					$('#WishlistBadge').html('');
				},
				timeout: 5000
			});
			
			setTimeout('UpdateBadge("Wishlist")', 5000);
		break;
		
		case 'UTorrent':
			$.ajax({
				method: 'get',
				url:    'api/utorrent/badge?token=' + GetCookie('HubToken'),
				
				success: function(html) {
					if(html.Complete && html.Incomplete) {
						$('#UTorrentBadge').html('<span class="badge dual rightbadge blue-badge">' + html.Incomplete + '</span><span class="badge dual leftbadge red-badge">' + html.Complete + '</span>');
					}
					else if(html.Incomplete) {
						$('#UTorrentBadge').html('<span class="badge single blue-badge">' + html.Incomplete + '</span>');
					}
					else if(html.Complete) {
						$('#UTorrentBadge').html('<span class="badge single red-badge">' + html.Complete + '</span>');
					}
				},
				error: function() {
					$('#UTorrentBadge').html('');
				},
				timeout: 5000
			});
			
			setTimeout('UpdateBadge("UTorrent")', 5000);
		break;
		
		case 'RSS':
			$.ajax({
				method: 'get',
				url:    'api/rss/badge?token=' + GetCookie('HubToken'),
				
				success: function(html) {
					for(RSSID in html) {
						$('#RSS-' + RSSID).html('<span class="badge single blue-badge">' + html[RSSID].Torrents + '</span>');
					}
				},
				error: function() {
					$('#RSS-' + RSSID).html('');
				},
				timeout: 5000
			});
			
			setTimeout('UpdateBadge("RSS")', 5000);
		break;
	}
}

function HubNotify(Type, Text) {
	var timeout = 0;
	switch(Type) {
		case 'success':
		case 'information':
			timeout = 5000;
		break;
	}
	
	noty({
		layout: 'topRight',
		type: Type,
		text: Text,
		timeout: timeout,
		theme: 'hub'
	});
}

function AjaxPost(Action, RowID) {
	switch(Action) {
		case 'WishlistAddItem':
			URL = 'wishlist';
		break;
		
		case 'DriveAddItem':
			URL = 'drives';
		break;
		
		case 'FeedAddItem':
			URL = 'rss';
		break;
	}
	
	switch(Action) {
		default:
			var Data = '{';
			$('#' + RowID).contents().each(function(index) {
				Name = $(this).contents().attr('name');
				Value = $(this).contents().attr('value');
				
				ImageObj = $(this).contents().find('img').first();
				
				if(Name != undefined && Value != undefined) {
					Data = Data + '"' + Name + '": "' + Value + '",';
				}
			});
			Data = Data.slice(0, -1) + '}';
	}
	
	$.ajax({
		type: 	'post',
		url:    'api/' + URL + '?token=' + GetCookie('HubToken'),
		data:   $.parseJSON(Data),
		beforeSend: function() {
			$(ImageObj).attr('src', 'images/spinners/ajax-light.gif');
		},
		success: function(data, textStatus, jqXHR) {
			$('#' + RowID).contents().each(function(index) {
				Name = $(this).contents().attr('name');
				Value = $(this).contents().attr('value');
				
				if(Name != undefined && Value != undefined) {
					$(this).html($(this).contents().attr('value'));
				}
			});
			
			$('#action-' + RowID).html('');
			
			HubNotify('success', data.error.message);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$(ImageObj).attr('src', 'images/icons/error.png');
		    
		    var responseObj = JSON.parse(jqXHR.responseText);
		    HubNotify('failure', responseObj.error.message);
		}
	});
}

function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 8;
	var randomstring = '';
	for(var i = 0; i < string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum+1);
	}
	
	return randomstring;
}

function AjaxButton(URL, ButtonObj, BeforeText, ButtonClass, ButtonVal, Method, Data) {
	if(Method == undefined) {
		Method = 'get';
	}
	
	$.ajax({
		type: 	Method,
		url:    '/api/' + URL + '?token=' + GetCookie('HubToken'),
		data:   Data,
		beforeSend: function() {
			$(ButtonObj).removeClass(ButtonClass).addClass('disabled');
			$(ButtonObj).contents().find('.label').text(BeforeText);
		},
		success: function(data, textStatus, jqXHR) {
			$(ButtonObj).removeClass('disabled').addClass(ButtonClass);
			$(ButtonObj).contents().find('.label').text(ButtonVal);
			
			HubNotify('success', data.error.message);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$(ButtonObj).removeClass('disabled').addClass(ButtonClass);
		    $(ButtonObj).contents().find('.label').text('Error!');
		    
		    var responseObj = JSON.parse(jqXHR.responseText);
		    HubNotify('failure', responseObj.error.message);
		}
	});
}

function AjaxImage(URL, ImageObj, OriginalImg, Method, Data) {
	if(Method == undefined) {
		Method = 'get';
	}
	
	$.ajax({
		type: 	Method,
		url:    'api/' + URL + '?token=' + GetCookie('HubToken'),
		data:   Data,
		beforeSend: function() {
			$(ImageObj).html('<img src="images/spinners/ajax-light.gif" />');
		},
		success: function(data, textStatus, jqXHR) {
			$(ImageObj).html(OriginalImg);		
			
			if(Method == 'delete') {
				$(ImageObj).parent().parent().remove();
			}
			
			HubNotify('success', data.error.message);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$(ImageObj).html('<img src="images/icons/error.png" />');
		    
		    var responseObj = JSON.parse(jqXHR.responseText);
		    HubNotify('failure', responseObj.error.message);
		}
	});
}
</script>

<table class="main">
 <tr>
  <td class="header left">
   <a href="/"><img src="images/logo.png" title="Hub v<?php echo json_decode($Hub->Request('/hub/version')); ?>" /></a>
   <img src="images/blank.gif" id="divider" />
  </td>
  <td class="header middle">
   <input type="search" id="search" placeholder="Search..." results="5" />
  </td>
  <td class="header right">
   <div id="jquery-wrapped-fine-uploader"></div>
  </td>
 </tr>
 <tr>
  <td id="navigation">
   <div id="navbuttons">
    <span id="LockStatus"></span>
    <span id="TorrentSpeedSetting"></span>
    <a href="?Page=Settings"><img id="IconSettings" src="images/icons/settings_dark.png" /></a>
    <a href="?Page=Profile"><img id="IconProfile" src="images/icons/profile_dark.png" /></a>
    <a href="?Page=Users"><img id="IconUsers" src="images/icons/users_dark.png" /></a>
   	<a href="?Page=Logout"><img id="IconLogout" src="images/icons/logout_dark.png" /></a>
   </div>
   <ul>
    <li class="series"><a href="?Page=Series">Series</a></li>
    <li class="wishlist"><a href="?Page=Wishlist">Wishlist</a><span id="WishlistBadge"></span></li>
    <li class="drive"><a href="?Page=Drives">Drives</a></li>
    <li class="extract"><a href="?Page=ExtractFiles">Extract Files</a></li>
    <li class="log"><a href="?Page=Log">Log</a></li>
    <li class="log"><a href="?Page=ScheduleLog">Schedule Log</a></li>
   </ul>
       
   <h3>uTorrent</h3>
   <ul>
   	<li class="control-panel"><a href="?Page=UTorrentCP">Control Panel</a><span id="UTorrentBadge"></span></li>
   </ul>
   
   <h3>RSS Feeds</h3>  
   <ul>
   	<li class="control-panel"><a href="?Page=RSSCP">Control Panel</a></li>
   	<?php 
   	$Feeds = json_decode($Hub->Request('rss/'));
   	
   	if(is_array($Feeds)) {
   		foreach($Feeds AS $Feed) {
   			echo '<li class="feeds"><a href="?Page=RSS&ID='.$Feed->ID.'">'.$Feed->Title.'</a><span id="RSS-'.$Feed->ID.'"></span></li>'."\n";
   		}
   	}
   	?>
   </ul>
  </td>
  <td colspan="2" id="maincontent">
   <?php
   if(filter_has_var(INPUT_GET, 'Page')) {
   	$Page = $_GET['Page'];
   }
   else {
   	$Page = '';
   }
   
   switch($Page) {
   	case 'Settings':
   		include_once APP_PATH.'/pages/Settings.php';
   	break;
   	
   	case 'Profile':
   		include_once APP_PATH.'/pages/Profile.php';
   	break;
   	
   	case 'Users':
   		include_once APP_PATH.'/pages/Users.php';
   	break;
   	
   	case 'Series':
   		include_once APP_PATH.'/pages/Series.php';
   	break;
   	
   	case 'Movies':
   		include_once APP_PATH.'/pages/Movies.php';
   	break;
   	
   	case 'Wishlist':
   		include_once APP_PATH.'/pages/Wishlist.php';
   	break;
   	
   	case 'Drives':
   		include_once APP_PATH.'/pages/Drives.php';
   	break;
   	
   	case 'FileManager':
   		include_once APP_PATH.'/pages/FileManager.php';
   	break;
   	
   	case 'ExtractFiles':
   		include_once APP_PATH.'/pages/ExtractFiles.php';
   	break;
   	
   	case 'Log':
   		include_once APP_PATH.'/pages/Log.php';
   	break;
   	
   	case 'ScheduleLog':
   		include_once APP_PATH.'/pages/ScheduleLog.php';
   	break;
   	
   	case 'UTorrentCP':
   		include_once APP_PATH.'/pages/UTorrentCP.php';
   	break;
   	
   	case 'RSSCP':
   		include_once APP_PATH.'/pages/RSSCP.php';
   	break;
   	
   	case 'RSS':
   		include_once APP_PATH.'/pages/RSS.php';
   	break;
   	
   	case 'Search':
   		include_once APP_PATH.'/pages/Search.php';
   	break;
   	
   	case 'Logout':
   		$Logout = json_decode($Hub->Request('users/logout/'));
   		setcookie('HubToken', '', time() - 3600);
   		header('Location: '.$_SERVER['HTTP_REFERER']);
   	break;
   	
   	case 'Schedule':
   	default:
   		include_once APP_PATH.'/pages/Schedule.php';
   }
   ?>
  </td>
 </tr>
</table>
<?php
}
?>

</body>	
</html> 