<?php
$sql = array();
$sql[] = "ALTER TABLE  `log` ADD  `Address` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER  `Date`";
$sql[] = "CREATE TABLE IF NOT EXISTS `tokens` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Date` int(10) NOT NULL,
  `Token` varchar(100) NOT NULL,
  `Expire` int(10) NOT NULL,
  `Active` int(10) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Client` varchar(100) NOT NULL,
  `UserKey` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
$sql[] = "ALTER TABLE  `user` CHANGE  `UserGroupKey`  `Level` ENUM(  'guest',  'user',  'admin' ) NOT NULL";
?>