<?php
class Users {
	private $PDO;
	
	function __construct() {
		$this->PDO = DB::Get();
	}
	
	/**
	 * @url    GET /
	 * @access protected
	 * @class  AccessControl {@Requires admin}
 	**/
	function UsersAll() {
		try {
			$UsersPrep = $this->PDO->prepare('SELECT
			                                  	*
			                                  FROM
			                                  	User');
		                                     	
			$UsersPrep->execute();
			$UsersRes = $UsersPrep->fetchAll();
			
			if(sizeof($UsersRes)) {
				return $UsersRes;
			}
			else {
				throw new RestException(404, 'Did not find any users matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    POST /
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function AddUser() {
		$RequiredParameters = array('Name',
		                            'Password',
		                            'EMail',
		                            'UserGroupKey');
		
		if(sizeof($_POST) != 4) {
			throw new RestException(412, 'Invalid request. Required parameters are "'.implode(', ', $RequiredParameters).'"');
		}
		
		foreach($_POST AS $Key => $Value) {
			if(!in_array($Key, $RequiredParameters)) {
				throw new RestException(412, 'Invalid request. Required parameters are "'.implode(', ', $RequiredParameters).'"');
			}
		}
		
		try {
			$UserCheckPrep = $this->PDO->prepare('SELECT
			                           	          	*
			                                      FROM
			                           	          	User
			                                      WHERE
			                           	          	Name = :Name
			                                      OR
			                           	          	EMail = :EMail');
			
			$UserCheckPrep->execute(array(':Name'  => $_POST['Name'],
			                              ':EMail' => $_POST['EMail']));
			$UserCheckRes = $UserCheckPrep->fetchAll();
			
			if(sizeof($UserCheckRes)) {
				throw new RestException(412, 'A user already exists with that information');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		if(!filter_var($_POST['UserEMail'], FILTER_VALIDATE_EMAIL)) {
			throw new RestException(412, '"'.$_POST['EMail'].'" is not a valid e-mail address');
		}
		
		try {
			$UserAddPrep = $this->PDO->prepare('INSERT INTO
													User
														(Date,
														Name,
														Password,
														EMail,
														UserGroupKey)
													VALUES
														(:Date,
														:Name,
														:EMail,
														:UserGroupKey)');
														
			$UserAddPrep->execute(array(':Date'     => time(),
			                            ':Name'     => $_POST['Name'],
			                            ':Password' => md5($_POST['Password']),
			                            ':EMail'    => $_POST['EMail'],
			                            ':UserGroupKey' => $_POST['UserGroupKey']));
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry = 'Added user "'.$_POST['Name'].'" with e-mail "'.$_POST['EMail'].'"';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(201, $LogEntry);
	}
	
	/**
	 * @url    GET /tokens
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function GetTokens() {
		if(!filter_has_var(INPUT_GET, 'token')) {
			throw new RestException(412);
		}
		
		try {
			$TokenPrep = $this->PDO->prepare('SELECT
												User.Name,
												USer.Level,
												Tokens.*
											  FROM
											  	User,
											  	Tokens
											  WHERE
											  	User.ID = Tokens.UserKey
											  ORDER BY
											  	Tokens.Active
											  DESC');
	
	    	$TokenPrep->execute();
	    	$TokenRes = $TokenPrep->fetchAll();
	    	
	    	if(sizeof($TokenRes)) {
	    		return $TokenRes;
	    	}
	    	else {
	    		throw new RestException(404, 'No tokens');
	    	}
	    }
	    catch(PDOException $e) {
	    	throw new RestException(400, 'MySQL: '.$e->getMessage());
	    }
	}
	
	/**
	 * @url POST /token
	**/
	function GetToken($Name, $Password, $Persistent = FALSE) {
		if(empty($Name) || empty($Password)) {
			throw new RestException(412, 'Invalid request. Required parameters are "Name", "Password"');
		}
		
		try {
			$UserPrep = $this->PDO->prepare('SELECT
			                                 	*
			                                 FROM
			                                  	User
			                                 WHERE
			                                  	Name = :Name
			                               	 AND
			                               	 	Password = :Password');
			                               	 	
			$UserPrep->execute(array(':Name'     => $Name,
									 ':Password' => md5($Password)));
			$UserRes = $UserPrep->fetch();
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		if(is_array($UserRes)) {
			try {
				$UserTokenPrep = $this->PDO->prepare('SELECT
														User.*,
														Tokens.Token 
												 	  FROM 
												  		User, 
												  		Tokens 
												 	  WHERE 
												  		User.ID = Tokens.UserKey 
												 	  AND 
												  		User.Name = :Name 
												 	  AND 
												  		User.Password = :Password 
												 	  ORDER BY 
												  		Active 
												 	  LIMIT 1');
														
				$UserTokenPrep->execute(array(':Name'     => $Name,
										 	  ':Password' => md5($Password)));
				$UserTokenRes = $UserTokenPrep->fetch();
			}
			catch(PDOException $e) {
				throw new RestException(400, 'MySQL: '.$e->getMessage());
			}
			
			if(is_array($UserTokenRes)) {
				return $UserTokenRes['Token'];
			}
			
			$Token = uniqid(strtolower($UserRes['Name']), TRUE);
			
			try {
				$TokenPrep = $this->PDO->prepare('INSERT INTO
													Tokens
														(Date,
														Token,
														Expire,
														Active,
														Address,
														Client,
														UserKey)
													VALUES
														(:Date,
														:Token,
														:Expire,
														:Active,
														:Address,
														:Client,
														:UserKey)');	
				$TokenPrep->execute(array(':Date'    => time(),
										  ':Token'   => $Token,
										  ':Expire'  => $Persistent ? 0 : strtotime('+1 hour'),
										  ':Active'  => time(),
										  ':Address' => $_SERVER['REMOTE_ADDR'],
										  ':Client'  => filter_has_var(INPUT_SERVER, 'HTTP_USER_AGENT') ? $_SERVER['HTTP_USER_AGENT'] : 'NA',
										  ':UserKey' => $UserRes['ID']));
			}
			catch(PDOException $e) {
				throw new RestException(400, '4MySQL: '.$e->getMessage());
			}
			
			return $Token;
		}
		
		throw new RestException(401);
	}
	
	/**
	 * @url GET /logout
	**/
	function Logout() {
		if(!filter_has_var(INPUT_GET, 'token')) {
			throw new RestException(412);
		}
		
		try {
			$TokenPrep = $this->PDO->prepare('DELETE FROM
												Tokens
											  WHERE
											  	Token = :Token');
	
	    	$TokenPrep->execute(array(':Token' => $_GET['token']));
	    }
	    catch(PDOException $e) {
	    	throw new RestException(400, 'MySQL: '.$e->getMessage());
	    }
	    
	    throw new RestException(200);
	}
	
	/**
	 * @url GET /validate
	**/
	function ValidateToken() {
		try {
			$TokenPrep = $this->PDO->prepare('SELECT
	    										User.Level,
	    										Tokens.*
	    									  FROM
	    										User,
	    										Tokens
	    									  WHERE
	    										Tokens.UserKey = User.ID
	    									  AND
	    										Tokens.Token = :Token
	    									  AND
	    										(Tokens.Expire > :Expire
	    											OR
	    										Tokens.Expire = 0)');
	
	    	$TokenPrep->execute(array(':Token'  => $_GET['token'],
	    							  ':Expire' => time()));
	    	$TokenRes = $TokenPrep->fetchAll();
	    }
	    catch(PDOException $e) {
	    	throw new RestException(400, 'MySQL: '.$e->getMessage());
	    }
	    
	    if(!sizeof($TokenRes)) {
	    	throw new RestException(401);
	    }
	    
	    throw new RestException(200);
	}
	
	/**
	 * @url    GET /groups
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function UserGroupsAll() {
		try {
			$UserGroupPrep = $this->PDO->prepare('SELECT
			                                      	*
			                                      FROM
			                                      	UserGroups');
			                                      	
			$UserGroupPrep->execute();
			$UserGroupRes = $UserGroupPrep->fetchAll();
			
			if(sizeof($UserGroupRes)) {
				return $UserGroupRes;
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		throw new RestException(404, 'Did not find any user groups');
	}
	
	/**
	 * @url    POST /groups
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function AddUserGroup() {
		$RequiredParameters = array('Name');
		
		if(sizeof($_POST) != 4) {
			throw new RestException(412, 'Invalid request. Required parameters are "'.implode(', ', $RequiredParameters).'"');
		}
		
		foreach($_POST AS $Key => $Value) {
			if(!in_array($Key, $RequiredParameters)) {
				throw new RestException(412, 'Invalid request. Required parameters are "'.implode(', ', $RequiredParameters).'"');
			}
		}
		
		try {
			$UserCheckPrep = $this->PDO->prepare('SELECT
			                           	          	*
			                                      FROM
			                           	          	UserGroups
			                                      WHERE
			                           	          	Name = :Name');
			
			$UserCheckPrep->execute(array(':Name' => $_POST['Name']));
			$UserCheckRes = $UserCheckPrep->fetchAll();
			
			if(sizeof($UserCheckRes)) {
				throw new RestException(412, 'A user group already exists with that information');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		try {
			$UserAddPrep = $this->PDO->prepare('INSERT INTO
													UserGroups
														(Date,
														Name)
													VALUES
														(:Date,
														:Name)');
														
			$UserAddPrep->execute(array(':Date' => time(),
			                            ':Name' => $_POST['Name']));
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry = 'Added user group "'.$_POST['Name'].'"';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(201, $LogEntry);
	}
	
	/**
	 * @url    GET /groups/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function GetUserGroup($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		try {
			$UserGroupPrep = $this->PDO->prepare('SELECT
			                                  	  	*
			                                      FROM
			                                  	  	UserGroups
			                                      WHERE
			                                   	  	ID = :ID');
			                                  	
			$UserGroupPrep->execute(array(':ID' => $ID));
			$UserGroupRes = $UserGroupPrep->fetchAll();
			
			if(sizeof($UserGroupRes)) {
				return $UserGroupRes;
			}
			else {
				throw new RestException(404, 'Did not find any user group in the database matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    DELETE /groups/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function DeleteUserGroup($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		$LogEntry = '';
		try {	
			$UserGroupDeletePrep = $this->PDO->prepare('DELETE FROM
												  	    	UserGroups
												        WHERE
												   	    	ID = :ID');
												   	
			$UserGroupDeletePrep->execute(array(':ID' => $ID));
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		try {	
			$UserDeletePrep = $this->PDO->prepare('DELETE FROM
												   	User
												   WHERE
												   	UserGroupKey = :ID');
												   	
			$UserDeletePrep->execute(array(':ID' => $ID));
			$DeletedUsers = $UserDeletePrep->rowCount();
			
			if($DeletedUsers) {
				$LogEntry .= 'Deleted '.$DeletedUsers.' belonging to the user group with the ID "'.$ID.'" from the database'."\n";
			}
			
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry .= 'Deleted user group with the ID "'.$ID.'" from the database';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(200, $LogEntry);
	}
	
	/**
	 * @url    POST /groups/update/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function UpdateUserGroup($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		$AcceptedParameters = array('Name',
		                            'PermissionKey');
		
		if(!sizeof($_POST)) {
			throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
		}
		
		$LogEntry = '';
		foreach($_POST AS $Key => $Value) {
			if(!in_array($Key, $AcceptedParameters)) {
				throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
			}
			
			if($Key == 'PermissionKey' && is_array($Value)) {
				try {
					$UserGroupPermissionsPrep = $this->PDO->prepare('DELETE FROM
					                                                 	UserGroupPermissions
					                                                 WHERE
					                                                 	UserGroupKey = :ID');
					                                                 
					$UserGroupPermissionsPrep->execute(array(':ID' => $ID));
				}
				catch(PDOException $e) {
					throw new RestException(400, 'MySQL: '.$e->getMessage());
				}
				
				foreach($Value AS $PermissionKey) {
					try {
						$UserGroupPermissionsPrep = $this->PDO->prepare('INSERT INTO
						                                                 	UserGroupPermissions
						                                                 		(UserGroupKey,
						                                                 		PermissionKey)
						                                                 	VALUES
						                                                 		(:ID,
						                                                 		:PermissionKey)');
						                                              
						$UserGroupPermissionsPrep->execute(array(':ID'            => $ID,
						                                         ':PermissionKey' => $PermissionKey));
						                                         
						$AddedPermissions = $UserGroupPermissionsPrep->rowCount();
						if($AddedPermissions) {
							$LogEntry .= 'User group with ID "'.$ID.'" now has '.$AddedPermissions.' permissions'."\n";
						}
					}
					catch(PDOException $e) {
						throw new RestException(400, 'MySQL: '.$e->getMessage());
					}
				}
			}
		}
		
		try {
			$UserGroupPrep = $this->PDO->prepare('UPDATE
			                                      	UserGroups
			                                      SET
			                                      	Name = :Name
			                                      WHERE
			                                      	ID = :ID');
			                                      	
			$UserGroupPrep->execute(array(':ID'            => $ID,
			                              ':Name' => $_POST['Name']));
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry .= 'Updated user group with the ID "'.$ID.'" in the database';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(200, $LogEntry);
	}
	
	/**
	 * @url    GET /permissions
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function PermissionsAll() {
		try {
			$PermissionsPrep = $this->PDO->prepare('SELECT
			                                  	  		*
			                                        FROM
			                                  	  		Permissions
			                                  	  	ORDER BY
			                                  	  		Text');
			                                  	
			$PermissionsPrep->execute();
			$PermissionsRes = $PermissionsPrep->fetchAll();
			
			if(sizeof($PermissionsRes)) {
				return $PermissionsRes;
			}
			else {
				throw new RestException(404, 'Did not find any permissions  in the database matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    POST /permissions
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function AddPermission() {
		$RequiredParameters = array('Action',
		                            'Text');
		
		if(sizeof($_POST) != 2) {
			throw new RestException(412, 'Invalid request. Required parameters are "'.implode(', ', $RequiredParameters).'"');
		}
		
		foreach($_POST AS $Key => $Value) {
			if(!in_array($Key, $RequiredParameters)) {
				throw new RestException(412, 'Invalid request. Required parameters are "'.implode(', ', $RequiredParameters).'"');
			}
		}
		
		try {
			$UserCheckPrep = $this->PDO->prepare('SELECT
			                           	          	*
			                                      FROM
			                           	          	Permissions
			                                      WHERE
			                           	          	Action = :Action
			                           	          OR
			                           	          	Text = :Text');
			
			$UserCheckPrep->execute(array(':Action' => $_POST['Action'],
			                              ':Text'   => $_POST['Text']));
			$UserCheckRes = $UserCheckPrep->fetchAll();
			
			if(sizeof($UserCheckRes)) {
				throw new RestException(412, 'A permission already exists with that information');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		try {
			$Permission = $this->PDO->query('SELECT COUNT(*) AS Total FROM Permissions')->fetch();
			
			$UserAddPrep = $this->PDO->prepare('INSERT INTO
													Permissions
														(Date,
														Action,
														Text,
														Value)
													VALUES
														(:Date,
														:Action,
														:Text,
														:Value)');
														
			$UserAddPrep->execute(array(':Date' => time(),
			                            ':Action' => $_POST['Action'],
			                            ':Text'   => $_POST['Text'],
			                            ':Value'  => pow(($Permission['Total'] + 1), 2)));
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry = 'Added permission "'.$_POST['Text'].'"';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(201, $LogEntry);
	}
	
	/**
	 * @url    DELETE /permissions/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function DeletePermission($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		try {
			$PermissionsDeletePrep = $this->PDO->prepare('DELETE FROM
			                                              	Permissions
			                                              WHERE
			                                              	ID = :ID');
			                                  	
			$PermissionsDeletePrep->execute(array(':ID' => $ID));
			
			$DeletedPermissions = $PermissionsDeletePrep->rowCount();
			if($DeletedPermissions) {
				$LogEntry = 'Deleted permission with ID "'.$ID.'" from the database';
				
				AddLog(EVENT.'Users', 'Success', $LogEntry);
				throw new RestException(200, $LogEntry);
			}
			else {
				throw new RestException(404, 'Did not find any permissions in the database matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    POST /permissions/update/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function UpdatePermission($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		$AcceptedParameters = array('Action',
		                            'Text');
		
		if(!sizeof($_POST)) {
			throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
		}
		
		$UpdateQuery = 'UPDATE Permissions SET ';
		$PrepArr = array();
		$i = 0;
		foreach($_POST AS $Key => $Value) {
			if(!in_array($Key, $AcceptedParameters)) {
				throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
			}
			
			$UpdateQuery .= ' '.$Key.' = :'.$Key;
			$PrepArr[':'.$Key] = $Value;
			
			if(++$i != sizeof($_POST)) {
				$UpdateQuery .= ', ';
			}
			else {
				$UpdateQuery .= ' WHERE ID = :ID';
				$PrepArr[':ID'] = $ID;
			}
		}
		
		try {
			$RSSPrep = $this->PDO->prepare($UpdateQuery);
			$RSSPrep->execute($PrepArr);
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry = 'Updated permission with the ID "'.$ID.'" in the database';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(200, $LogEntry);
	}
	
	/**
	 * @url    GET /me
	 * @access protected
	 * @class  AccessControl {@Requires user}
	**/
	function GetLoggedInUser() {
		if(filter_has_var(INPUT_GET, 'token')) {
			$Token = $_GET['token'];
		}
		else if(filter_has_var(INPUT_COOKIE, 'HubToken')) {
			$Token = $_COOKIE['HubToken'];
		}
		else {
			throw new RestException(404, 'No such token');
		}
		
		try {
			$UserPrep = $this->PDO->prepare('SELECT
			                                  User.ID,
			                                  User.Date,
			                                  User.Name,
			                                  User.EMail,
			                                  User.Level
			                                 FROM
			                                  User,
			                                  Tokens
			                                 WHERE
			                                  User.ID = Tokens.UserKey
			                                 AND
			                                  Tokens.Token = :Token');
			                                  	
			$UserPrep->execute(array(':Token' => $Token));
			$UserRes = $UserPrep->fetch();
			
			if(is_array($UserRes)) {
				return $UserRes;
			}
			else {
				throw new RestException(404, 'Did not find any user in the database matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    GET /:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function GetUser($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		try {
			$UserPrep = $this->PDO->prepare('SELECT
			                                  *
			                                 FROM
			                                  User
			                                 WHERE
			                                  ID = :ID');
			                                  	
			$UserPrep->execute(array(':ID' => $ID));
			$UserRes = $UserPrep->fetch();
			
			if(is_array($UserRes)) {
				return $UserRes;
			}
			else {
				throw new RestException(404, 'Did not find any user in the database matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    DELETE /:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function DeleteUser($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		$LogEntry = '';
		try {	
			$UserDeletePrep = $this->PDO->prepare('DELETE FROM
												   	User
												   WHERE
												   	ID = :ID');
												   	
			$UserDeletePrep->execute(array(':ID' => $ID));
			$DeletedUser = $UserDeletePrep->rowCount();
			
			if($DeletedUser) {
				$LogEntry .= 'Deleted user with the ID "'.$ID.'" from the database'."\n";
			}
			
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(200, $LogEntry);
	}
	
	/**
	 * @url    POST /update/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function UpdateUser($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		$AcceptedParameters = array('Name',
		                            'Password',
		                            'EMail',
		                            'UserGroupKey');
		
		if(!sizeof($_POST)) {
			throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
		}
		
		$UpdateQuery = 'UPDATE User SET ';
		$PrepArr = array();
		$i = 0;
		foreach($_POST AS $Key => $Value) {
			if(!in_array($Key, $AcceptedParameters)) {
				throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
			}
			
			$UpdateQuery .= ' '.$Key.' = :'.$Key;
			
			if($Key == 'UserEMail') {
				if(!filter_var($_POST['UserEMail'], FILTER_VALIDATE_EMAIL)) {
					throw new RestException(412, 'Invalid request. "'.$_POST['EMail'].'" is not a valid e-mail address');
				}
				
				$PrepArr[':'.$Key] = $Value;
			}
			else if($Key == 'UserPassword') {
				$PrepArr[':'.$Key] = md5($Value);
			}
			else {
				$PrepArr[':'.$Key] = $Value;
			}
			
			if(++$i != sizeof($_POST)) {
				$UpdateQuery .= ', ';
			}
			else {
				$UpdateQuery .= ' WHERE ID = :ID';
				$PrepArr[':ID'] = $ID;
			}
		}
		
		try {
			$RSSPrep = $this->PDO->prepare($UpdateQuery);
			$RSSPrep->execute($PrepArr);
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry = 'Updated user with the ID "'.$ID.'" in the database';
		
		AddLog(EVENT.'Users', 'Success', $LogEntry);
		throw new RestException(200, $LogEntry);
	}
}
?>