<?php
class Wishlist {
	private $PDO;
	
	function __construct() {
		$this->PDO = DB::Get();
	}
	
	/**
	 * @url    GET /
	 * @access protected
	 * @class  AccessControl {@Requires user}
	**/
	function WishlistAll() {
		try {
			$WishlistPrep = $this->PDO->prepare('SELECT
		                                     	 	*
		                                     	 FROM
		                                     	 	Wishlist
		                                     	 ORDER BY
		                                     	 	Title');
		                                     	
			$WishlistPrep->execute();
			$WishlistRes = $WishlistPrep->fetchAll();
			
			if(sizeof($WishlistRes)) {
				return $WishlistRes;
			}
			else {
				throw new RestException(404, 'Did not find any wishlist items matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    GET /new
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function GetFulfilledWishesSinceLastActivity() {
		try {
			$WishlistPrep = $this->PDO->prepare('SELECT
		                                     	 	*
		                                     	 FROM
		                                     	 	Wishlist
		                                     	 WHERE
		                                     	 	DownloadDate > :LastActivity
		                                     	 ORDER BY
		                                     	 	Title');
		                                     	
			$WishlistPrep->execute(array(':LastActivity' => GetActivity(1, '/api/wishlist/reset')));
			$WishlistRes = $WishlistPrep->fetchAll();
			
			if(sizeof($WishlistRes)) {
				return $WishlistRes;
			}
			else {
				throw new RestException(404, 'Did not find any wishlist items matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    GET /reset
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function ResetFulfilledWishesSinceLastActivity() {
		throw new RestException(200, 'Reset');
	}
	
	/**
	 * @url    GET /refresh
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function WishlistRefresh() {
		$DrivesObj = new Drives;
		$Movies    = $DrivesObj->GetMovieFiles();
		
		try {
			$WishlistRefreshPrep = $this->PDO->prepare('UPDATE
			                                            	Wishlist
			                                            SET
			                                            	File = null,
			                                            	IsFileGone = 1
			                                            WHERE
			                                            	DownloadDate != 0
			                                            AND
			                                            	File != ""
			                                            OR
			                                            	(DownloadDate != 0
			                                            AND
			                                            	TorrentKey != 0)');
			                                            	
			$WishlistRefreshPrep->execute();
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$WishlistItems = 0;
		foreach($Movies AS $Movie) {
			$ParsedInfo = ParseRelease($Movie);
			
			if(is_array($ParsedInfo)) {
				try {
					$WishItemPrep = $this->PDO->prepare('SELECT
					                                     	*
					                                     FROM
					                                     	Wishlist
					                                     WHERE
					                                     	Title = :Title
					                                     AND
					                                     	Year = :Year');
					                                     	
					$WishItemPrep->execute(array(':Title' => $ParsedInfo['Title'],
												 ':Year'  => $ParsedInfo['Year']));
					$WishlistItem = $WishItemPrep->fetch();
				}
				catch(PDOException $e) {
					throw new RestException(400, 'MySQL: '.$e->getMessage());
				}
				
				if(is_array($WishlistItem)) {
					if(!$WishlistItem['DownloadDate']) {
						$WishlistDownloadDate = time();
					}
					else {
						$WishlistDownloadDate = $WishlistItem['DownloadDate'];
					}
					
					try {
						$WishlistRefreshPrep = $this->PDO->prepare('UPDATE
						                                            	Wishlist
						                                            SET
						                                            	File = :File,
						                                            	IsFileGone = 0,
						                                            	DownloadDate = :Date
						                                            WHERE
						                                            	Title = :Title
						                                            AND
						                                            	Year = :Year');
						                                            	
						$WishlistRefreshPrep->execute(array('File'  => $Movie,
														    'Date'  => $WishlistDownloadDate,
														    'Title' => $ParsedInfo['Title'],
														    'Year'  => $ParsedInfo['Year']));
														
						$WishlistItems++;
					}
					catch(PDOException $e) {
						throw new RestException(400, 'MySQL: '.$e->getMessage());
					}
				}
			}
		}
		
		try {
			$UpdatePrep = $this->PDO->prepare('UPDATE
			                                   	Hub
			                                   SET
			                                   	Value = :Time
			                                   WHERE
			                                   	Setting = "LastWishlistRefresh"');
			                                   	
			$UpdatePrep->execute(array(':Time' => time()));
			
			if($WishlistItems) {
				$LogEntry = 'Refreshed '.$WishlistItems.' wishlist items';
				
				AddLog(EVENT.'Wishlist', 'Success', $LogEntry);
				throw new RestException(200, $LogEntry);
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    GET /badge
	 * @access protected
	 * @class  AccessControl {@Requires guest}
	**/
	function GetBadge() {
		$Badge = array();
		$WishlistPrep = $this->PDO->prepare('SELECT
												COUNT(ID) 
											 AS 
											 	Incomplete 
											 FROM 
											 	Wishlist 
											 WHERE 
											 	DownloadDate = ""');
											 	
		$WishlistPrep->execute();
		$WishlistResult = $WishlistPrep->fetch();
		
		if($WishlistResult['Incomplete']) {
			$Badge['Incomplete'] = $WishlistResult['Incomplete'];
		}
		
		$WishlistPrep = $this->PDO->prepare('SELECT 
												COUNT(ID) 
											 AS 
											 	Complete 
											 FROM 
											 	Wishlist 
											 WHERE 
											 	DownloadDate > :LastActivity');
											 	
		$WishlistPrep->execute(array(':LastActivity' => GetActivity(1, '/api/wishlist/')));
		$WishlistResult = $WishlistPrep->fetch();
		
		if($WishlistResult['Complete']) {
			$Badge['Complete'] = $WishlistResult['Complete'];
		}
		
		return $Badge;
	}
	
	/**
	 * @url    GET /imdb
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function AddWishlistFromIMDb($IMDb) {
		$IMDb = urldecode($_GET['IMDb']);
	
		require_once(APP_PATH.'/api/libraries/api.imdb.php');
		
		$oIMDB = new IMDB($IMDb);
		if($oIMDB->isReady) {
			$this->AddWishlistItem(ConvertCase(StripIllegalChars($oIMDB->getTitle())), $oIMDB->getYear());
		}
		else {
		    throw new RestException(404, $LogEntry);
		}
	}
	
	/**
	 * @url    POST /
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function AddWishlistItem($Title, $Year) {
		if(empty($Title) || empty($Year)) {
			throw new RestException(412, 'Invalid request. Required parameters are "Title", "Year"');
		}
		
		$Title = ConvertCase(StripIllegalChars($Title));
		
		try {
			$WishlistPrep = $this->PDO->prepare('SELECT
			                                     	ID
			                                     FROM
			                                     	Wishlist
			                                     WHERE
			                                     	Title = :Title
			                                     AND
			                                     	Year = :Year');
	
			$WishlistPrep->execute(array(':Title' => $Title,
			                             ':Year'  => $Year));
			$WishlistResult = $WishlistPrep->fetchAll();
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		if(sizeof($WishlistResult)) {
			throw new RestException(412, 'A wishlist item titled "'.$Title.' ('.$Year.')" already exists');
		}
		
		try {
			$WishlistAddPrep = $this->PDO->prepare('INSERT INTO
														Wishlist
															(Date,
															Title,
															Year)
														VALUES
															(:Date,
															:Title,
															:Year)');
														
			$WishlistAddPrep->execute(array(':Date'  => time(),
			                                ':Title' => $Title,
			                                ':Year'  => $Year));
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		$LogEntry = 'Added "'.$Title.' ('.$Year.')" to the wishlist';
		
		AddLog(EVENT.'Wishlist', 'Success', $LogEntry);
		throw new RestException(201, $LogEntry);
	}
	
	/**
	 * @url    DELETE /:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function DeleteWishlistItem($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		try {
			$WishlistPrep = $this->PDO->prepare('SELECT
													Title,
													Year
												 FROM
													Wishlist
												 WHERE
												 	ID = :ID');
			                            	
			$WishlistPrep->execute(array(':ID' => $ID));
			$WishlistRes = $WishlistPrep->fetch();
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		if(is_array($WishlistRes)) {
			try {
				$WishlistDeletePrep = $this->PDO->prepare('DELETE FROM
				                            	           	Wishlist
				                                           WHERE
				                            	            ID = :ID');
				                            	
				$WishlistDeletePrep->execute(array(':ID' => $ID));
			}
			catch(PDOException $e) {
				throw new RestException(400, 'MySQL: '.$e->getMessage());
			}
			
			$LogEntry = 'Deleted wishlist item "'.$WishlistRes['Title'].' ('.$WishlistRes['Year'].')" from the database';
			
			AddLog(EVENT.'Wishlist', 'Success', $LogEntry);
			throw new RestException(200, $LogEntry);
		}
		else {
			throw new RestException(404, 'Could not find a wishlist item with that ID in the database');
		}
	}
	
	/**
	 * @url    POST /update/:ID
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function UpdateWishlistItem($ID) {
		if(!is_numeric($ID)) {
			throw new RestException(412, 'ID must be a numeric value');
		}
		
		$AcceptedParameters = array('Title',
		                            'Year');
		
		if(!sizeof($_POST)) {
			throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
		}
		
		try {
			$WishlistPrep = $this->PDO->prepare('SELECT
													Title,
													Year
												 FROM
												 	Wishlist
												 WHERE
												 	ID = :ID');
			
			$WishlistPrep->execute(array(':ID' => $ID));
			$WishlistRes = $WishlistPrep->fetch();
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
		
		if(is_array($WishlistRes)) {
			$UpdateQuery = 'UPDATE Wishlist SET ';
			$PrepArr = array();
			$i = 0;
			foreach($_POST AS $Key => $Value) {
				if(!in_array($Key, $AcceptedParameters)) {
					throw new RestException(412, 'Invalid request. Accepted parameters are "'.implode(', ', $AcceptedParameters).'"');
				}
				
				$UpdateQuery .= ' '.$Key.' = :'.$Key;
				$PrepArr[':'.$Key] = $Value;
				
				if(++$i != sizeof($_POST)) {
					$UpdateQuery .= ', ';
				}
				else {
					$UpdateQuery .= ' WHERE ID = :ID';
					$PrepArr[':ID'] = $ID;
				}
			}
			
			try {
				$WishlistPrep = $this->PDO->prepare($UpdateQuery);
				$WishlistPrep->execute($PrepArr);
			}
			catch(PDOException $e) {
				throw new RestException(400, 'MySQL: '.$e->getMessage());
			}
			
			$LogEntry = 'Updated wishlist item "'.$WishlistRes['Title'].' ('.$WishlistRes['Year'].')"';
			
			AddLog(EVENT.'Wishlist', 'Success', $LogEntry);
			throw new RestException(200, $LogEntry);
		}
		else {
			throw new RestException(404);
		}
	}
}
?>