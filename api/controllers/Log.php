<?php
class Log {
	private $PDO;
	
	function __construct() {
		$this->PDO = DB::Get();
	}
	
	/**
	 * @url    GET /
	 * @url    GET /days/:Days
	 * @url    GET /lines/:Lines
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function LogAll($Days = 1, $Lines = 0) {
		if(!is_numeric($Days) || !is_numeric($Lines)) {
			throw new RestException(412, 'Number of Days or Lines must be a numeric value');
		}
		else {
			if($Days) {
				$LogQuery = 'SELECT
				             	*
				             FROM
				             	Log
				             WHERE
				             	Date > '.strtotime('-'.$Days.' days').'
				             AND
				             	Event != "Schedule/Hub"
				             ORDER BY
				             	ID
				             DESC';
			}
			else {
				$LogQuery = 'SELECT
				             	*
							 FROM
							 	Log
							 ORDER BY
							 	ID
							 DESC
							 LIMIT '.$Lines;
			}
		}
		
		try {
			$LogPrep = $this->PDO->prepare($LogQuery);
			$LogPrep->execute();
			$LogRes = $LogPrep->fetchAll();
			
			if(sizeof($LogRes)) {
				return $LogRes;
			}
			else {
				throw new RestException(404, 'Did not find anything in the log matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
	
	/**
	 * @url    GET /schedule
	 * @url    GET /schedule/days/:Days
	 * @url    GET /schedule/lines/:Lines
	 * @access protected
	 * @class  AccessControl {@Requires admin}
	**/
	function ScheduleLogAll($Days = 1, $Lines = 0) {
		if(!is_numeric($Days) || !is_numeric($Lines)) {
			throw new RestException(412, 'Number of Days or Lines must be a numeric value');
		}
		else {
			if($Days) {
				$LogQuery = 'SELECT
				             	*
				             FROM
				             	Log
				             WHERE
				             	Date > '.strtotime('-'.$Days.' days').'
				             AND
				             	Event = "Schedule/Hub"
				             ORDER BY
				             	ID
				             DESC';
			}
			else {
				$LogQuery = 'SELECT
				             	*
							 FROM
							 	Log
							 ORDER BY
							 	ID
							 DESC
							 LIMIT '.$Lines;
			}
		}
		
		try {
			$LogPrep = $this->PDO->prepare($LogQuery);
			$LogPrep->execute();
			$LogRes = $LogPrep->fetchAll();
			
			if(sizeof($LogRes)) {
				return $LogRes;
			}
			else {
				throw new RestException(404, 'Did not find anything in the log matching your criteria');
			}
		}
		catch(PDOException $e) {
			throw new RestException(400, 'MySQL: '.$e->getMessage());
		}
	}
}
?>