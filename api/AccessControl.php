<?php
use \Luracast\Restler\iAuthenticate;
use \Luracast\Restler\Resources;

class AccessControl implements iAuthenticate {
    public static $Requires = 'user';
    public static $Role     = '';
    public static $Roles = array('guest' => array('access' => 0),
    							 'user'  => array('access' => 1),
    							 'admin' => array('access' => 2));

	private $PDO;
	
	function __construct() {
		$this->PDO = DB::Get();
	}

    public function __isAllowed() {
    	$Token = (filter_has_var(INPUT_GET, 'token')) ? $_GET['token'] : FALSE;
    	
    	try {
    		$TokenPrep = $this->PDO->prepare('DELETE FROM
    											Tokens
    										  WHERE
    										  	(Expire < :Expire
    										  		AND
    										  	Expire != 0)
    										  OR
    										  	Active < :Active');
    
        	$TokenPrep->execute(array(':Expire' => time(),
        							  ':Active' => strtotime('-31 days')));
        }
        catch(PDOException $e) {
        	throw new RestException(400, 'MySQL: '.$e->getMessage());
        }
    	
    	try {
    		$TokenPrep = $this->PDO->prepare('SELECT
        										User.Level,
        										User.ID AS UserID,
        										Tokens.*
        									  FROM
        										User,
        										Tokens
        									  WHERE
        										Tokens.UserKey = User.ID
        									  AND
        										Tokens.Token = :Token
        									  AND
        										(Tokens.Expire > :Expire
        											OR
        										Tokens.Expire = 0)');
    
        	$TokenPrep->execute(array(':Token'  => $Token,
        							  ':Expire' => time()));
        	$TokenRes = $TokenPrep->fetch();
        }
        catch(PDOException $e) {
        	throw new RestException(400, 'MySQL: '.$e->getMessage());
        }
    	
    	if(is_array($TokenRes)) {
    		LogActivity($TokenRes['UserID'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    		
    		try {
    			$TokenPrep = $this->PDO->prepare('UPDATE
    												Tokens
    											  SET
    											  	Active  = :Active,
    											  	Address = :Address
    											  WHERE
    											  	Token = :Token');
    			
    			$TokenPrep->execute(array(':Active'  => time(),
    									  ':Address' => $_SERVER['REMOTE_ADDR'],
    									  ':Token'   => $Token));
    		}
    		catch(PDOException $e) {
    			throw new RestException(400, 'MySQL: '.$e->getMessage());
    		}
    		
    		static::$Role = $TokenRes['Level'];
    	}
    	else {
    		return FALSE;
    	}
        
        if(static::$Roles[static::$Role]['access'] >= static::$Roles[static::$Requires]['access']) {
        	return TRUE;
        }
        
        return FALSE;
    }
}
?>