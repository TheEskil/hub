<?php
ini_set('display_errors', 1);
ini_set('max_execution_time', (60 * 60 * 5));
header('Access-Control-Allow-Origin: http://www.imdb.com');

if(PHP_SAPI == 'cli') {
	define('EVENT', 'Schedule/');
}
else {
	define('EVENT', '');
}

define('APP_PATH', realpath(dirname(__FILE__).'/../'));

require_once 'resources/DB.php';
require_once 'resources/functions.php';
require_once 'vendor/restler.php';
use Luracast\Restler\Restler;

$Directories = array('controllers/');

foreach(glob('controllers/*.php') AS $Filename) {
    require_once $Filename;
}

$Restler = new Restler();
$Restler->setSupportedFormats('JsonFormat', 'UploadFormat');
$Restler->addAuthenticationClass('AccessControl');
$Restler->addAPIClass('Drives');
$Restler->addAPIClass('Hub');
$Restler->addAPIClass('Log');
$Restler->addAPIClass('RSS');
$Restler->addAPIClass('Series');
$Restler->addAPIClass('Settings');
$Restler->addAPIClass('Users');
$Restler->addAPIClass('UTorrent');
$Restler->addAPIClass('Wishlist');
$Restler->handle();
?>