<?php
define('APP_PATH', realpath(dirname(__FILE__).'/'));

require_once APP_PATH.'/resources/qqFileUploader.php';
require_once APP_PATH.'/api/resources/DB.php';
require_once APP_PATH.'/api/resources/functions.php';

$UploadFolder = GetSetting('UTorrentWatchFolder');

if(!$UploadFolder) {
	echo json_encode(array('error' => 'Failed to get uTorrent watch folder from the database'));
	
	return;
}

if(!is_dir($UploadFolder)) {
	echo json_encode(array('error' => $UploadFolder.' does not exist'));
	
	return;
}

$uploader = new qqFileUploader();

// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
$uploader->allowedExtensions = array('torrent');

// Specify max file size in bytes.
$uploader->sizeLimit = 2 * 1024 * 1024;

// Specify the input name set in the javascript.
$uploader->inputName = 'qqfile';

// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
$result = $uploader->handleUpload('c:/Torrents');

// To save the upload with a specified name, set the second parameter.
// $result = $uploader->handleUpload('uploads/', md5(mt_rand()).'_'.$uploader->getName());

// To return a name used for uploaded file you can use the following line.
$result['uploadName'] = $uploader->getUploadName();

header("Content-Type: text/plain");
echo json_encode($result);
?>