<?php
error_reporting(E_ALL);

ini_set('display_errors',     1); 
ini_set('log_errors',         1);
ini_set('error_log',          realpath(dirname(__FILE__)).'/tmp/schedule_error.log');
ini_set('max_execution_time', (60 * 60 * 5));

session_start();

require_once realpath(dirname(__FILE__)).'/resources/config.php';
require_once realpath(dirname(__FILE__)).'/resources/cli.php';
require_once realpath(dirname(__FILE__)).'/api/resources/DB.php';
require_once realpath(dirname(__FILE__)).'/api/resources/functions.php';
require_once realpath(dirname(__FILE__)).'/resources/api.hub.php';

$Output = array();

$Token = json_decode($Hub->Request('/users/token', 'POST', array('Name' => SCHEDULE_USER, 'Password' => SCHEDULE_PASS, 'Persistent' => TRUE)));
$Output[] = ScheduleOutput('Token', $Token);

if(is_object($Token) && is_object($Token->error)) {
	AddLog('Schedule/Hub', 'Failure', json_encode($Output));
	die('unable to get token');
}
else {
	$Hub->SetToken($Token);
	
	$Output[] = ScheduleOutput('Database Upgrade', json_decode($Hub->Request('/hub/upgrade')));
	
	$LockStatus = json_decode($Hub->Request('/hub/lockstatus'));
	$Output[] = ScheduleOutput('Lock Status', $LockStatus);
	
	if(is_object($LockStatus) && property_exists($LockStatus, 'error')) {
		if($LockStatus->error->code == 200) {
			$Output[] = ScheduleOutput('Lock', json_decode($Hub->Request('/hub/lock')));
		}
		else {
			AddLog('Schedule/Hub', 'Failure', json_encode($Output));
			die('locked');
		}
	}
	
	// Check for existing active drive and that all required folders are present
	$ActiveDrive = json_decode($Hub->Request('/drives/active/check'));
	$Output[] = ScheduleOutput('Active Drive', $ActiveDrive);
	
	if(is_object($ActiveDrive) && property_exists($ActiveDrive, 'error')) {
		if($ActiveDrive->error->code != 200) {
			AddLog('Schedule/Hub', 'Failure', json_encode($Output));
			die('no active drive');
		}
	}
	
	// Update RSS Feeds
	$Output[] = ScheduleOutput('RSS Refresh', json_decode($Hub->Request('/rss/refresh')));
	
	// Download torrents corresponding with new episodes and/or wishlist items
	$Output[] = ScheduleOutput('Download Torrents', json_decode($Hub->Request('/rss/download')));
	
	// Remove finished torrents from uTorrent
	$Output[] = ScheduleOutput('Remove Finished Torrents', json_decode($Hub->Request('/utorrent/remove/finished')));
	
	// Delete backup files older than x days as defined in Hub settings
	$Output[] = ScheduleOutput('Clean Backup Folder', json_decode($Hub->Request('/hub/backup/clean')));
	
	// Extract and/or move completed downloads across all drives
	$CompletedFiles = json_decode($Hub->Request('/drives/files/completed'));
	$Output[] = ScheduleOutput('Files Completed Downloading', $CompletedFiles);
	
	if(is_object($CompletedFiles) && !property_exists($CompletedFiles, 'error')) {
		if(is_object($CompletedFiles) && property_exists($CompletedFiles, 'Move')) {
			foreach($CompletedFiles->Move AS $Move) {
				json_decode($Hub->Request('/drives/files/move', 'POST', array('File' => $Move)));
			}
		}
		
		if(is_object($CompletedFiles) && property_exists($CompletedFiles, 'Extract')) {
			foreach($CompletedFiles->Extract AS $Extract) {
				json_decode($Hub->Request('/drives/files/extract', 'POST', array('File' => $Extract)));
			}
		}
	}
	
	// Delete empty folders and irrelevant files from the downloads folder
	$CleanedDownloadsFolder = json_decode($Hub->Request('/drives/clean'));
	$Output[] = ScheduleOutput('Cleaned Downloads Folder', $CleanedDownloadsFolder);
	
	$FolderRebuild   = GetSetting('LastFolderRebuild');
	$SerieRefresh    = GetSetting('LastSerieRefresh');
	$SerieRebuild    = GetSetting('LastSerieRebuild');
	$WishlistRefresh = GetSetting('LastWishlistRefresh');
	$Backup          = time();//GetSetting('LastBackup');
	
	$LatestUpdate = min($FolderRebuild, $SerieRefresh, $SerieRebuild, $WishlistRefresh, $Backup);
	if((date('G') >= 4 && date('G') <= 6) || (time() - $LatestUpdate) >= (60 * 60 * 24 * 2)) {
		if(date('dmy', $FolderRebuild) != date('dmy')) {
			$Output[] = ScheduleOutput('Series: Rebuild Folders', json_decode($Hub->Request('/series/rebuild/folders')));
		}
	
		if(date('dmy', $SerieRefresh) != date('dmy')) {
			$Output[] = ScheduleOutput('Series: Refresh', json_decode($Hub->Request('/series/refresh/all')));
		}
	
		if(date('dmy', $SerieRebuild) != date('dmy')) {
			$Output[] = ScheduleOutput('Series: Rebuild Episodes', json_decode($Hub->Request('/series/rebuild/episodes')));
		}
		
		if(date('dmy', $WishlistRefresh) != date('dmy')) {
			$Output[] = ScheduleOutput('Wishlist: Refresh', json_decode($Hub->Request('/wishlist/refresh')));
		}
	}
	
	/*
	$LatestUpdate = min($FolderRebuild, $SerieRefresh, $SerieRebuild, $WishlistUpdate, $WishlistRefresh, $Backup);
	if((date('G') >= 4 && date('G') <= 6) || (time() - $LatestUpdate) >= (60 * 60 * 24 * 2)) {
		if(date('dmy', $Backup) != date('dmy')) {
			if(is_dir($HubObj->GetSetting('BackupFolder'))) {
				if($HubObj->GetSetting('BackupHubFiles')) {
					if(!is_file($HubObj->GetSetting('BackupFolder').'/hub-files-'.date('d-m-Y').'.zip')) {
						if($HubObj->ZipDirectory(APP_PATH, $HubObj->GetSetting('BackupFolder').'/hub-files-'.date('d-m-Y').'.zip')) {
							$HubObj->AddLog(EVENT.'Backup', 'Success', 'Backed up Hub files to "'.$HubObj->GetSetting('BackupFolder').'/hub-files-'.date('d-m-Y').'.zip"');
						}
					}
				}
			
				if($HubObj->GetSetting('BackupHubDatabase')) {
					$HubObj->BackupDatabase(DB_USER, DB_PASS, DB_NAME, $HubObj->GetSetting('BackupFolder'));
				}
			}
		}
	}
	*/
	
	$Unlock = json_decode($Hub->Request('/hub/unlock'));
	$Output[] = ScheduleOutput('Unlock', $Unlock);
	
	if(is_object($Unlock) && property_exists($Unlock, 'error')) {
		if($Unlock->error->code != 200) {
			AddLog(EVENT.'Hub', 'Failure', 'Failed to remove lock');
		}
	}
	
	AddLog('Schedule/Hub', 'Success', json_encode($Output));
}
?>